import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import RazasScreen from './src/screens/Razas';
import Modal from './src/screens/Modal';

const OnBoardingNavigator = createStackNavigator(
  {
    Razas: {
      screen: RazasScreen,
    },
  },
  {
    initialRouteName: 'Razas',
  },
);
const AppNavigator = createStackNavigator(
  {
    Razas: {
      screen: RazasScreen,
    },
  },
  {
    initialRouteName: 'Razas',
  },
);

const RootStack = createStackNavigator(
  {
    Main: AppNavigator,
    Modal: Modal,
  },
  {
    mode: 'modal',
    headerMode: 'none',
  },
);

const BaseStack = createSwitchNavigator(
  {
    OnBoarding: OnBoardingNavigator,
    Root: RootStack,
  },
  {
    initialRouteName: 'OnBoarding',
  },
);

export default createAppContainer(BaseStack);
