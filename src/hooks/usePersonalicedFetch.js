import {useEffect, useState} from 'react';

const usePersonalicedFetch = (url) => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const fetchData = async () => {
    const response = await fetch(url);
    const data = await response.json();

    var arr1 = Object.keys(data.message);
    const razas = [];
    arr1.forEach((item, i) => {
      const [letter] = item.split(',');
      const raza = {id: i.toString(), raza: letter};
      razas.push(raza);
    });

    var data2 = {data: razas};
    setData(razas);
    setLoading(false);
  };
  useEffect(() => {
    fetchData();
  }, []);
  return {loading, data};
};

export default usePersonalicedFetch;
