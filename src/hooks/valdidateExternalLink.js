export function validateExternalLink(url) {
  const validToOpen = ['dog.ceo'];

  if (url.indexOf('://') > -1) {
    url = url.split('/')[2];
  }

  const targetDomain = url.split('/')[0];

  return validToOpen.includes(targetDomain);
}
