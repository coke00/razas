import React from 'react';
import {View, StyleSheet, FlatList, Text} from 'react-native';
import ListItem from '../components/ListItem';
import usePersonalicedFetch from '../hooks/usePersonalicedFetch';

const Razas = ({navigation}) => {
  const {loading, data: razas} = usePersonalicedFetch(
    'https://dog.ceo/api/breeds/list/all',
  );
  return (
    <View style={styles.container}>
      {loading ? (
        <Text>Cargando...</Text>
      ) : (
        <FlatList
          style={styles.list}
          data={razas}
          keyExtractor={(x) => x.id}
          renderItem={({item}) => (
            <ListItem
              onPress={() => navigation.navigate('Modal', {raza: item.raza})}
              name={item.raza}
            />
          )}
        />
      )}
    </View>
  );
};
Razas.navigationOptions = {
  title: 'Razas de perros',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  list: {
    alignSelf: 'stretch',
  },
});

export default Razas;
