import React from 'react';
import {View, Text, StyleSheet, Button, Image, FlatList} from 'react-native';
import useFetch from '../hooks/useFetch';

const Modals = ({navigation}) => {
  const raza = navigation.getParam('raza');
  const {loading, data} = useFetch(`https://dog.ceo/api/breed/${raza}/images`);
  return (
    <View style={styles.container}>
      {loading ? (
        <Text>Cargando ...</Text>
      ) : (
        <>
          <FlatList
            style={styles.list}
            data={data}
            keyExtractor={(item, index) => '' + index}
            renderItem={({item}) => (
              <Image
                source={{uri: item}}
                style={{
                  width: 260,
                  height: 300,
                  borderWidth: 2,
                  borderColor: '#d35647',
                  resizeMode: 'contain',
                  margin: 8,
                }}
              />
            )}
          />
          <Button
            title="Cancelar"
            onPress={() => navigation.navigate('Razas')}
          />
        </>
      )}
    </View>
  );
};

Modals.navigationOptions = {
  title: 'Modal',
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Modals;
