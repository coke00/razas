import {renderHook, act} from '@testing-library/react-hooks';
import usePersonalicedFetch from '../src/hooks/usePersonalicedFetch';
test('debe validar el url', () => {
  const {result} = renderHook(() =>
    usePersonalicedFetch('https://dog.ceo/api/breeds/list/all'),
  );
  expect(result.current.data).toStrictEqual([]);
});
